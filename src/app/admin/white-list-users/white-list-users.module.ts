import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { WhiteListUsersComponent } from './white-list-users.component';

import { ModalModule } from 'ngx-bootstrap';

import { FabModule } from '../fab/fab.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FabModule,
    ModalModule.forRoot()
  ],

  declarations: [
    WhiteListUsersComponent
  ]
})

export class WhiteListUsersModule { }
