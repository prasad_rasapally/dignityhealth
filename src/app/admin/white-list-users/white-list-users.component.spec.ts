import { APP_CONFIG, APP_DI_CONFIG } from './../../app.config';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Http, Response, Headers, HttpModule, ConnectionBackend, RequestOptions, BaseRequestOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { WhiteListUsersComponent } from './white-list-Users.component';
import { WhiteListUser } from '../../modals/white-list-users.modal';
import { NewWhiteListUser } from '../../modals/new-white-list-user.modal';
import { WhiteListService } from '../services/white-list.service';
import { ModalModule } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { FabModule } from '../fab/fab.module';

import { FormBuilder, FormGroup, FormsModule, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { Globals } from "app/globals";

describe('WhiteListUsersComponent', () => {
  let component: WhiteListUsersComponent;
  let fixture: ComponentFixture<WhiteListUsersComponent>;
  let item;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, ModalModule.forRoot(), FabModule],
      declarations: [WhiteListUsersComponent],
      providers: [
        { provide: ConnectionBackend, useClass: MockBackend },
        { provide: RequestOptions, useClass: BaseRequestOptions },
        WhiteListService,
        BsModalRef,
        BsModalService,
        Http,
        Globals,
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhiteListUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sortWithFirstName must be false', () => {
    expect(component.sortWithFirstName).toEqual(false);
  });

  it('sortWithLastName  must be false', () => {
    expect(component.sortWithLastName).toEqual(false);
  });

  it('Show First Name Sort Options function must be true', () => {
    component.showFirstNameSortOptions()
    expect(component.sortWithFirstName).toEqual(true);
  });

  it('Show Last Name Sort Options function must be true', () => {
    component.showLastNameSortOptions()
    expect(component.sortWithLastName).toEqual(true);
  });

  it('Show Filter Options function must be true', () => {
    this.filterWithStatus = false;
    component.showFilterOptions()
    expect(component.filterWithStatus).toEqual(!this.filterWithStatus);
  });
  
  it('isActive function must contain the value item', () => {
    component.selectedClass("testitem");
    expect(component.isActive("testitem")).toBeTruthy();
  });

  it('selectedClass function must contain the value item', () => {
    component.selectedClass("testitem")
    expect(component.selectedItem).toEqual("testitem");
  });

  it('selectedClass function must contain the value item', () => {
    component.selectedClass("testitem")
    expect(component.selectedItem).toEqual("testitem");
  });
});