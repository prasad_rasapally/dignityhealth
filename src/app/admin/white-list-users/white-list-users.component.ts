import { Component, OnInit } from '@angular/core';

import { WhiteListUser } from '../../modals/white-list-users.modal';

import { NewWhiteListUser } from '../../modals/new-white-list-user.modal';

import { WhiteListService } from '../services/white-list.service';

import { WhiteListUserService } from '../services/white-list-user.service';

import { Globals } from '../../globals';

@Component({
  selector: 'app-white-list-users',
  templateUrl: './white-list-users.component.html',
  styleUrls: ['./white-list-users.component.css']
})
export class WhiteListUsersComponent implements OnInit {

  selectedItem: any;

  whiteListUsers: Array<any>;

  filteredUsers: Array<any>;

  userModal: NewWhiteListUser;

  sortWithFirstName: boolean;

  sortWithLastName: boolean;

  filterWithStatus: boolean;

  constructor(

    public whiteListService: WhiteListService,

    public whiteListUserService: WhiteListUserService,

    public globals: Globals

  ) {}

  ngOnInit() {    

    this.sortWithFirstName = false;

    this.sortWithLastName = false;

    this.filterWithStatus = false;

    this.globals.loading = true;

    this.getWhiteListUsers();

  };

  getWhiteListUsers() {

    this.whiteListUserService.getWhiteListUsers();

  };

  sortUsers(sortOrder, columnProperty) : void {

    this.whiteListUserService.sortUsers(sortOrder, columnProperty);

  }

  filterByStatus(selectedStatus: string): void {

    this.whiteListUserService.filterByStatus(selectedStatus);
    
  }

  showFirstNameSortOptions(): void {

    this.sortWithFirstName = !this.sortWithFirstName;

    this.sortWithLastName = false;

    this.filterWithStatus = false;

  }

  showLastNameSortOptions(): void {

    this.sortWithLastName = !this.sortWithLastName;

    this.sortWithFirstName = false;

    this.filterWithStatus = false;

  }

  showFilterOptions(): void {

    this.filterWithStatus = !this.filterWithStatus;

    this.sortWithFirstName = false;

    this.sortWithLastName = false;

  }

  selectedClass(item): void {

    this.selectedItem = item;

  };

  isActive(item): any {

    return this.selectedItem == item;

  };

  userOnBlurListener(): void {
    this.filterWithStatus = false;
    this.sortWithFirstName = false;
    this.sortWithLastName = false;
  }

}


