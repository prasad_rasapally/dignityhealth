import { Component, OnInit, ViewChild } from '@angular/core';

import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { BsModalService } from 'ngx-bootstrap/modal';

import { WhiteListService } from '../../services/white-list.service';

import { WhiteListUserService } from '../../services/white-list-user.service';

import { Globals } from '../../../globals';

import {
  UserSubmitFeedbackComponent
} from '../../feedback/user-submit-feedback.component';

@Component({
  selector: 'app-invalid-records-validation',
  templateUrl: './invalid-records-validation.component.html',
  styleUrls: ['./invalid-records-validation.component.css']
})
export class InvalidRecordsValidationComponent implements OnInit {

  usersForm: FormGroup;

  users: FormArray;

  formErrors: Array<any>;

  validationMessages: any;

  Applications: String[] = ['BNI Curbside Submitter'];

  Prefixes: String[] = ['Dr.', 'Jr.', 'Mr.', 'Mrs.'];

  validRecordsCount: number;

  invalidRecordsCount: number;

  validRecords: Array<any>;

  invalidRecords: Array<any>;

  constructor(

    public formBuilder: FormBuilder,

    public bsModalService: BsModalService,

    public bsModalRef: BsModalRef,

    public whiteListService: WhiteListService,

    public whiteListUserService: WhiteListUserService,

    public globals: Globals

  ) { }

  ngOnInit() {

    this.validRecords = JSON.parse(localStorage.getItem('inputRecords'));

    this.invalidRecords = JSON.parse(localStorage.getItem('invalidRecords'));

    this.invalidRecordsCount = this.invalidRecords.length;

    this.validRecordsCount = this.validRecords.length;

    this.buildUserForm();
  }

  buildUserForm(): void {

    this.formErrors = [];

    this.usersForm = this.formBuilder.group({

      users: this.formBuilder.array(this.createUser())

    });

    this.usersForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    //this.onValueChanged(); // (re)set validation messages now    

    this.validationMessages = {

      'Application': {
        'required': 'Incomplete Entry'
      },

      'EmailAddress': {
        'required': 'Incomplete Entry',
        'pattern': 'Invalid Email'
      },

      'Prefix': {
        'required': 'Incomplete Entry'
      },

      'FirstName': {
        'required': 'Incomplete Entry',
        'pattern': 'Only Characters And Numbers Are Allowed',
        'maxlength': 'Maximum length for First Name is 25'
      },

      'LastName': {
        'required': 'Incomplete Entry',
        'pattern': 'Only Characters And Numbers Are Allowed',
        'maxlength': 'Maximum length for Last Name is 25'
      }

    };
  };

  createUser(): Array<FormGroup> {

    let UsersForm: Array<FormGroup> = [];

    let invalidRecords = JSON.parse(localStorage.getItem('invalidRecords'));

    invalidRecords.forEach((element, index) => {
      UsersForm.push(
        this.formBuilder.group({
          Application: [element.Application, Validators.required],
          EmailAddress: [element.EmailAddress, Validators.required],
          Prefix: [element.Prefix, Validators.required],
          FirstName: [element.FirstName, Validators.required],
          LastName: [element.LastName, Validators.required]
        })
      );

      this.formErrors.push(
        {
          'Application': element.Application ? '' : 'Incomplete Entry',
          'EmailAddress': !element.EmailAddress ? 'Incomplete Entry' : !this.validateEmail(element.EmailAddress) ? "Invalid Email" : '',
          'Prefix': element.Prefix ? '' : 'Incomplete Entry',
          'FirstName': element.FirstName ? '' : 'Incomplete Entry',
          'LastName': element.LastName ? '' : 'Incomplete Entry',
        }
      );
    }
    );

    return UsersForm;

  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  getUsersFormGroup(field: string): any {

    return this.usersForm.get('' + field);

  };

  onValueChanged(data?: any): void {

    if (!this.usersForm) { return; }

    const formLength = this.formErrors.length;

    const self = this;

    for (let i = 0; i < formLength; i++) {

      if (self.getUsersFormGroup('users').controls) {

        self.getUsersFormGroup('users').controls.forEach((element, index) => {

          for (let subfield in element.controls) {

            self.formErrors[i][subfield] = '';

            let control = element.controls[subfield];

            if (control && control && control.invalid) {

              let messages = self.validationMessages[subfield];

              for (let key in control.errors) {

                self.formErrors[index][subfield] = messages[key] + '';
                //console.log(self.formErrors);

              }

            }
          }
        });
      } else {

        return;

      }
    }

  };

  removeInvalidUser(index: number): void {

    this.users = this.usersForm.get('users') as FormArray;

    this.users.removeAt(index);

    this.formErrors.splice(index, 1);

    this.onValueChanged();

  };

  mergeAndUploadBatchRecords(usersForm, flag: boolean) {

    this.globals.loading = true;

    this.bsModalRef.hide();

    let mergedRecords = this.validRecords.concat(usersForm.users);

    let request: Array<any> = this.createRequest(mergedRecords, flag);

    let length = request ? request.length : 0;

    if (!length) {

      this.globals.loading = false;

      return;

    }

    this.whiteListService.addWhiteListUser(request)
      .subscribe(response => {

        this.globals.loading = false;

        this.bsModalRef = this.bsModalService.show(UserSubmitFeedbackComponent);

        this.bsModalRef.content.title = 'Input Batch File';

        this.bsModalRef.content.feedback = response.Message;

        setTimeout(() => {

          this.bsModalRef.hide();          

          this.whiteListUserService.refreshWhiteListUsers();

        },

          2000

        );

        localStorage.clear();

      },
      error => {
        this.handleError();
      })
  }


  handleError(): void {

    this.globals.loading = false;

    localStorage.clear();

    console.log('handle error here');

  };

  createRequest(mergedRecords: any, flag: boolean): Array<any> {

    let request = [];

    mergedRecords.forEach(element => {

      request.push(
        {
          "Prefix": element.Prefix,
          "FirstName": element.FirstName,
          "LastName": element.LastName,
          "Email": element.EmailAddress,
          "AppId": 1,
          "Invite": flag
        }
      );

    });

    return request;
  };

}
