import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvalidRecordsValidationComponent } from './invalid-records-validation.component';

describe('InvalidRecordsValidationComponent', () => {
  let component: InvalidRecordsValidationComponent;
  let fixture: ComponentFixture<InvalidRecordsValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvalidRecordsValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvalidRecordsValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
