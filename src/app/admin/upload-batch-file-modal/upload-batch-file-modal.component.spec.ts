import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadBatchFileModalComponent } from './upload-batch-file-modal.component';
import { FileSelectDirective } from 'ng2-file-upload';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ComponentLoaderFactory } from 'ngx-bootstrap/component-loader';
import { PositioningService } from 'ngx-bootstrap/positioning';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { FormBuilder, FormGroup, FormsModule, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from "ngx-bootstrap";

describe('UploadBatchFileModalComponent', () => {
  let component: UploadBatchFileModalComponent;
  let fixture: ComponentFixture<UploadBatchFileModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, ModalModule.forRoot()],
      declarations: [ UploadBatchFileModalComponent, FileSelectDirective ],
      providers : [
        BsModalService,
        ComponentLoaderFactory,
        PositioningService,
        BsModalRef
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadBatchFileModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
