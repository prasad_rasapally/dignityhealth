import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import * as XLSX from 'xlsx';

import { WhiteListService } from '../services/white-list.service';

import { WhiteListUserService } from '../services/white-list-user.service';

import { Globals } from '../../globals';

import {
  UserSubmitFeedbackComponent
} from '../feedback/user-submit-feedback.component';

import {
  InvalidRecordsValidationComponent
} from './invalid-records-validation/invalid-records-validation.component';

@Component({
  selector: 'app-upload-batch-file-modal',
  templateUrl: './upload-batch-file-modal.component.html',
  styleUrls: ['./upload-batch-file-modal.component.css']
})
export class UploadBatchFileModalComponent implements OnInit {

  validFileExtensions = ['xls', 'xlsx', 'ods'];

  batchFileForm: FormGroup;

  selectedFile: any;

  rABS: boolean = false;

  workbook: XLSX.WorkBook;

  inputRecords: any;

  invalidRecords: any;

  fileFormatError: string = "";

  constructor(

    public bsModalService: BsModalService,

    public bsModalRef: BsModalRef,

    public formBuilder: FormBuilder,

    public whiteListService: WhiteListService,

    public whiteListUserService: WhiteListUserService,

    public globals: Globals

  ) { }

  ngOnInit() {

    this.selectedFile = "";

  }

  handleOnFileSelect(event): void {

    let files = event.target.files;

    this.selectedFile = files[0].name;

    let splitArray = this.selectedFile.split('.');

    let extension = splitArray[splitArray.length - 1];

    if (this.validFileExtensions.indexOf(extension) == -1) {

      this.fileFormatError = "Unsupported File Format. Please Upload .xls or .xlsx or .ods File.";

    } else {

      this.fileFormatError = "";

    }

    var i, f;

    for (i = 0; i != files.length; ++i) {

      f = files[i];

      var reader = new FileReader();

      reader.onload = (e) => {

        let target: any = e.target;

        let data: string = target.result;

        if (this.rABS) {

          /* if binary string, read with type 'binary' */
          this.workbook = XLSX.read(data, { type: 'binary' });

        } else {

          /* if array buffer, convert to base64 */
          var arr = this.fixData(data);

          this.workbook = XLSX.read(btoa(arr), { type: 'base64' });

        }

        /* DO SOMETHING WITH workbook HERE */

        this.inputRecords = JSON.parse(this.to_json(this.workbook));

        this.inputRecords =
          this.inputRecords.Sheet1 ?
            this.inputRecords.Sheet1 :
            this.inputRecords['Sample-spreadsheet-file'];

        if (this.inputRecords) {

          this.invalidRecords = this.validateRecordsAtFrontEnd(this.inputRecords);

        }

        localStorage.setItem('inputRecords', JSON.stringify(this.inputRecords));
        localStorage.setItem('invalidRecords', JSON.stringify(this.invalidRecords));

        //console.log('inputRecords ', this.inputRecords);

        //console.log('invalidRecords ', this.invalidRecords);

        if (this.invalidRecords.length) {
          this.showInvalidRecordsForUpdate();
        }

      };

      reader.readAsArrayBuffer(f);

    }

  };

  showInvalidRecordsForUpdate() {

    this.bsModalRef.hide();

    this.bsModalService.show(InvalidRecordsValidationComponent, { backdrop: 'static' });

  };

  validateRecordsAtFrontEnd(inputRecords: any): Array<any> {

    let invalidRecords: Array<any> = [], length = inputRecords.length;

    if (!length) { return [] }

    let fields = ["Prefix", "FirstName", "LastName", "EmailAddress", "Application"];

    let fieldsLength = fields.length;

    let invalidIndexes = [];

    inputRecords.forEach((element, index) => {

      let tempRecord = {
        "Prefix": element.Prefix,
        "FirstName": element.FirstName,
        "LastName": element.LastName,
        "EmailAddress": element.EmailAddress,
        "Application": element.Application
      };

      let invalid: boolean = false;

      for (let i = 0; i < fieldsLength; i++) {

        if (!(fields[i] in element)) {

          tempRecord[fields[i]] = "";

          invalid = true;

        }

        if (fields[i] == "EmailAddress" && !this.validateEmail(element[fields[i]])) {

          tempRecord[fields[i]] = element[fields[i]];

          invalid = true;

        }

      }

      if (invalid) {

        for (let attr in element) { tempRecord[attr] = element[attr]; }

        invalidRecords.push(tempRecord);

        invalidIndexes.push(index);

      }

    });

    if(invalidIndexes.length){

      invalidIndexes.forEach((value, index) => {

        inputRecords.splice(value - index, 1);

      });
      
    }

    return invalidRecords;
  };

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  fixData(data) {
    var o = "", l = 0, w = 10240;
    for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
    o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
    return o;
  };

  to_json(workbook) {
    var result = {};

    workbook.SheetNames.forEach(function (sheetName) {

      var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName]);

      if (roa.length) result[sheetName] = roa;

    });

    return JSON.stringify(result);

  };

  addNewWhiteListUsers(flag: boolean): void {

    this.globals.loading = true;

    this.bsModalRef.hide();

    let request: Array<any> = this.createRequest(flag);

    let length = request ? request.length : 0;

    if (!length) {

      this.globals.loading = false;

      return;

    }

    this.whiteListService.addWhiteListUser(request)
      .subscribe(response => {

        this.globals.loading = false;

        this.bsModalRef = this.bsModalService.show(UserSubmitFeedbackComponent);

        this.bsModalRef.content.title = 'Input Record(s)';

        this.bsModalRef.content.feedback = response.Message;

        setTimeout(() => {

          this.bsModalRef.hide();

          this.whiteListUserService.refreshWhiteListUsers();

        },

          2000

        );

      },
      error => {
        this.handleError();
      })

  };

  handleError(): void {

    this.globals.loading = false;

    console.log('handle error here');

  }

  createRequest(flag: boolean): Array<any> {

    let request = [];

    let sheet = this.inputRecords;

    let length = sheet ? sheet.length : 0;

    if (!length) { return; }

    sheet.forEach(element => {

      request.push(
        {
          "Prefix": element.Prefix,
          "FirstName": element.FirstName,
          "LastName": element.LastName,
          "Email": element.EmailAddress,
          "AppId": 1,
          "Invite": flag
        }
      );

    });

    return request;
  };
}
