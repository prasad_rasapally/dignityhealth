import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FabComponent } from './fab.component';
import { ModalModule } from "ngx-bootstrap";

@NgModule({
    imports: [ CommonModule,  ModalModule.forRoot() ],
    declarations: [ FabComponent ],
    exports: [ FabComponent ]
})
export class FabModule {}