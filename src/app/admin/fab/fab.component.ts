import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import {
  AddNewUserModalComponent
} from '../add-new-user-modal/add-new-user-modal.component';

import {
  UploadBatchFileModalComponent
} from '../upload-batch-file-modal/upload-batch-file-modal.component';

import { BsModalService } from 'ngx-bootstrap/modal';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'fab-button',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.css']
})
export class FabComponent implements OnInit {

  @Output()
  refreshUsersList: EventEmitter<any> = new EventEmitter();

  public bsModalRef: BsModalRef;

  public toggleFab: boolean = false;

  constructor(

    public bsModalService: BsModalService,

    public bsModalService1: BsModalService

  ) { }

  ngOnInit() { }

  showUserModalPopup(): void {

    this.toggleFab = !this.toggleFab;

    this.bsModalRef =
      this.bsModalService1.show(AddNewUserModalComponent, { backdrop: 'static' });

    this.bsModalRef.content.title = 'Add Record(s)';

  }

  showBatchFileModalPopup(): void {

    this.toggleFab = !this.toggleFab;

    this.bsModalRef =
      this.bsModalService.show(UploadBatchFileModalComponent, { backdrop: 'static' });

    this.bsModalRef.content.title = 'Upload Batch File';

  }

  toggleFloatingActionButtons(): void {
    this.toggleFab = !this.toggleFab;
  }
  fabOnblurListener(): void {
    this.toggleFab = false;
  }
}


