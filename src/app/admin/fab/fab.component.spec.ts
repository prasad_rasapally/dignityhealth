import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { FabComponent } from './fab.component';
import { ComponentLoaderFactory } from 'ngx-bootstrap/component-loader';
import { PositioningService } from 'ngx-bootstrap/positioning';

describe('FabComponent', () => {
  let component: FabComponent;
  let fixture: ComponentFixture<FabComponent>;
  let toggleFab: boolean = false
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabComponent ],
      providers: [
        BsModalRef,
        BsModalService,
      ComponentLoaderFactory,
      PositioningService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should Toggle Floating Action Buttons', () => {
    component.toggleFloatingActionButtons()
    expect(component.toggleFab).toEqual(!this.toggleFab);
  });
});
