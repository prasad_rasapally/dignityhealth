import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  
})
export class NavigationComponent implements OnInit {

  navigationShowHide: boolean;

  // navToggle: boolean;
  public navToggle: boolean = false;

  constructor() {
    this.navigationShowHide = true;
  //  this.navToggle = false;
  }

  ngOnInit() {
  }

  changeNavigationStatus(): void {
    this.navigationShowHide = !this.navigationShowHide;
  }

  toggleNav(){
		this.navToggle = !this.navToggle;
  }

}
