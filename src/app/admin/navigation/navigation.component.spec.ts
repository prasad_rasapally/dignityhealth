import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationComponent } from './navigation.component';

describe('NavigationComponent', () => {
  let component: NavigationComponent;
  let fixture: ComponentFixture<NavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

 it('Navigation Show Hide the values', () => {
    this.navigationShowHide = true
    component.changeNavigationStatus()
    expect(component.navigationShowHide).toEqual(!this.navigationShowHide);
  });
  it('Navigation toggle', () => {
    this.navToggle = false;
    component.toggleNav()
    expect(component.navToggle).toEqual(!this.navToggle);
  });
});


