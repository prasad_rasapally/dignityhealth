import { TestBed, inject } from '@angular/core/testing';

import { WhiteListUserService } from './white-list-user.service';

describe('WhitelistUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WhiteListUserService]
    });
  });

  it('should ...', inject([WhiteListUserService], (service: WhiteListUserService) => {
    expect(service).toBeTruthy();
  }));
});
