import {TestBed, inject} from '@angular/core/testing';

import { Http, Response, Headers, HttpModule, ConnectionBackend, RequestOptions, BaseRequestOptions } from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';

import { WhiteListService } from './white-list.service';

import { APP_CONFIG } from '../../app.config';
import { APP_DI_CONFIG } from '../../app.config';
import { AppConfig } from '../.././app.config.interface';

describe('WhiteListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {provide: ConnectionBackend, useClass: MockBackend},
        {provide: RequestOptions, useClass: BaseRequestOptions},
        WhiteListService,
        Http,
        {
          provide: APP_CONFIG, 
          useValue: APP_DI_CONFIG 
        }
      ]
    });
  });

  it('should ...', inject([WhiteListService], (service: WhiteListService) => {
    expect(service).toBeTruthy();
  }));
});
