import { Injectable } from '@angular/core';

import { WhiteListService } from './white-list.service';

import { Globals } from '../../globals';

@Injectable()
export class WhiteListUserService {

  whiteListUsers : Array<any>; 

  filteredUsers : Array<any>; 

  totalUsers: number = 0;

  sortOrder: number;

  columnProperty: string;

  constructor(

    public whiteListService: WhiteListService,

    public globals: Globals

  ) { }

  getWhiteListUsers() {

    this.globals.loading = true;

    this.whiteListService.getWhiteListUsers()
      .subscribe(result => {

        this.whiteListUsers = result.Data.Users;

        this.filteredUsers = this.whiteListUsers;

        this.filteredUsers.forEach((element, index) =>{

          switch(element.StatusID) {
            case 1: 
            element.Status = "Added";
            break;
            case 2: 
            element.Status = "Invited";
            break;
            case 3: 
            element.Status = "Active";
            break;
            case 4: 
            element.Status = "InActive";
            break;
          }          
        })

        this.totalUsers = this.whiteListUsers.length;

        this.sortUsers(1, "LastName");

        this.globals.loading = false;

      },
      error => {

        this.globals.loading = false;

        this.handleError();

      })
  };

  handleError(){
    console.log('handle error here');
  };

  refreshWhiteListUsers(){
    this.getWhiteListUsers();
  }

  sortUsers(sortOrder, columnProperty) {

    this.sortOrder = sortOrder;

    this.columnProperty = columnProperty;

    this.filteredUsers.sort((user1, user2) => {

      if (user1[columnProperty].toLocaleLowerCase() < user2[columnProperty].toLocaleLowerCase()) {

        return -1 * sortOrder;

      } else if (user1[columnProperty].toLocaleLowerCase() > user2[columnProperty].toLocaleLowerCase()) {

        return 1 * sortOrder;

      } else {

        return 0;

      }
    });
  }

  filterByStatus(selectedStatus: string): void {

    if (selectedStatus.toLocaleLowerCase() == "all") {

      this.filteredUsers = this.whiteListUsers;

      this.totalUsers = this.filteredUsers.length;

      this.sortUsers(this.sortOrder, this.columnProperty);

      return;

    }

    this.filteredUsers = this.whiteListUsers.filter((user) => {

      return user.Status.toLocaleLowerCase() == selectedStatus.toLocaleLowerCase();

    });

    this.sortUsers(this.sortOrder, this.columnProperty);

    this.totalUsers = this.filteredUsers.length;
  }

}
