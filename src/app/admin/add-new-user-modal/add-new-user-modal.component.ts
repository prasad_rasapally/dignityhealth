import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';

import { BsModalService } from 'ngx-bootstrap/modal';

import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { WhiteListService } from '../services/white-list.service';

import { WhiteListUserService } from '../services/white-list-user.service';

import {
  UserSubmitFeedbackComponent
} from '../feedback/user-submit-feedback.component';

import { Globals } from '../../globals';

@Component({
  selector: 'app-add-new-user-modal',
  templateUrl: './add-new-user-modal.component.html',
  styleUrls: ['./add-new-user-modal.component.css']
})
export class AddNewUserModalComponent implements OnInit {

  usersForm: FormGroup;

  users: FormArray;

  formErrors: Array<any>;

  validationMessages: any;

  Applications: String[] = ['BNI Curbside Submitter'];

  Prefixes: String[] = ['Dr.', 'Jr.', 'Mr.', 'Mrs.'];

  constructor(

    public whiteListService: WhiteListService,

    public whiteListUserService: WhiteListUserService,

    public bsModalService: BsModalService,

    public bsModalRef: BsModalRef,

    public formBuilder: FormBuilder,

    public globals: Globals

  ) { }

  ngOnInit() {

    this.buildUserForm();

  }

  buildUserForm(): void {

    this.usersForm = this.formBuilder.group({
      users: this.formBuilder.array([this.createUser()])

    });

    this.usersForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    //this.onValueChanged(); // (re)set validation messages now

    this.formErrors = [
      {
        'Application': '',
        'EmailAddress': '',
        'Prefix': '',
        'FirstName': '',
        'LastName': ''
      }
    ];

    this.validationMessages = {

      'Application': {
        'required': 'Application is Required.'
      },

      'EmailAddress': {
        'required': 'Email Address is Required.',
        'pattern': 'Wrong Email Pattern.'
      },

      'Prefix': {
        'required': 'Prefix is Required.'
      },

      'FirstName': {
        'required': 'First Name is Required.',
        'pattern': 'Only Characters And Numbers Are Allowed',
        'maxlength': 'Maximum length for First Name is 25'
      },

      'LastName': {
        'required': 'Last Name is Required.',
        'pattern': 'Only Characters And Numbers Are Allowed',
        'maxlength': 'Maximum length for Last Name is 25'
      }

    };
  };

  addUser(): void {

    this.users = this.usersForm.get('users') as FormArray;

    if (this.users.length < 10) { //maximum records to add is 10

      this.users.push(this.createUser());

      this.formErrors.push(this.createError());

    }

    return;
  };

  removeUser(index: number): void {

    this.users = this.usersForm.get('users') as FormArray;

    this.users.removeAt(index);

    this.formErrors.splice(index, 1);

  };

  createUser(): FormGroup {
    return this.formBuilder.group({
      Application: ['BNI Curbside Submitter', Validators.required],
      EmailAddress: ['', Validators.required],
      Prefix: ['Dr.', Validators.required],
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required]
    });
  };

  createError(): any {
    return {
      'Application': '',
      'EmailAddress': '',
      'Prefix': '',
      'FirstName': '',
      'LastName': ''
    };
  };

  getUsersFormGroup(field: string): any {

    return this.usersForm.get('' + field);

  };

  onValueChanged(data?: any): void {

    if (!this.usersForm) { return; }

    let formLength = this.formErrors.length;

    let self = this;

    for (let i = 0; i < formLength; i++) {

      if (self.getUsersFormGroup('users').controls) {

        self.getUsersFormGroup('users').controls.forEach((element, index) => {

          for (let subfield in element.controls) {

            self.formErrors[i][subfield] = '';

            let control = element.controls[subfield];

            if (control && control.dirty && !control.valid) {

              let messages = self.validationMessages[subfield];

              for (let key in control.errors) {

                self.formErrors[index][subfield] = messages[key] + '';
                //console.log(self.formErrors);

              }

            }
          }
        });
      } else {

        return;

      }
    }

  };


  addNewWhiteListUsers(usersForm: FormGroup, flag: boolean): void {

    this.globals.loading = true;

    this.bsModalRef.hide();

    let request: Array<any> = this.createRequest(usersForm, flag);

    this.whiteListService.addWhiteListUser(request)
      .subscribe(response => {

        this.globals.loading = false;

        this.bsModalRef = this.bsModalService.show(UserSubmitFeedbackComponent);

        this.bsModalRef.content.title = 'Input Record(s)';

        this.bsModalRef.content.feedback = response.Message;

        setTimeout(() => {

          this.bsModalRef.hide();

          this.whiteListUserService.refreshWhiteListUsers();

        },

          2000

        );

      },
      error => {
        this.handleOnError();
      })

  };

  handleOnError(): void {

    this.globals.loading = false;

    console.log('handle error here');

  };

  createRequest(usersForm: any, flag: boolean): Array<any> {

    let request = [];

    usersForm.users.forEach(element => {

      request.push(
        {
          "Prefix": element.Prefix,
          "FirstName": element.FirstName,
          "LastName": element.LastName,
          "Email": element.EmailAddress,
          "AppId": 1,
          "Invite": flag
        }
      );

    });

    return request;
  };

}
