import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddNewUserModalComponent } from './add-new-user-modal.component';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { FormBuilder, FormGroup, FormsModule, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { WhiteListService } from '../services/white-list.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Http, Response, Headers, HttpModule, ConnectionBackend, RequestOptions, BaseRequestOptions } from '@angular/http';
import { Globals } from "app/globals";
import { APP_CONFIG, APP_DI_CONFIG } from './../../app.config';

describe('AddNewUserModalComponent', () => {
  let component: AddNewUserModalComponent;
  let fixture: ComponentFixture<AddNewUserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, ModalModule.forRoot()],
      declarations: [ AddNewUserModalComponent ],
      providers: [
        { provide: ConnectionBackend, useClass: MockBackend },
        { provide: RequestOptions, useClass: BaseRequestOptions },
        WhiteListService,
        BsModalRef,
        BsModalService,
        Http,
        Globals,
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
