import { Component, OnInit } from '@angular/core';

import { WhiteListDomains } from '../../modals/white-list-domains.modal';

import { WhiteListService } from '../services/white-list.service';

import { BsModalService } from 'ngx-bootstrap/modal';

import { WhiteListDomainModalComponent } from './white-list-domain-modal/white-list-domain-modal.component';

import { Globals } from '../../globals';

@Component({
  selector: 'app-white-list-domains',
  templateUrl: './white-list-domains.component.html',
  styleUrls: ['./white-list-domains.component.css']
})

export class WhiteListDomainsComponent implements OnInit {

  whiteListDomains: Array<WhiteListDomains>;

  totalDomains : number = 0;

  constructor(
    public whiteListService : WhiteListService,

    public bsModalService: BsModalService,

    public globals : Globals
  ) { }

  ngOnInit() {

    this.globals.loading = true;

    this.getWhiteListDomains();

  };

  public openWhiteListDomainModal() {

    this.bsModalService.show( WhiteListDomainModalComponent );

  };

  getWhiteListDomains(){

     this.whiteListService.getWhiteListDomains()
    .subscribe(response => {

      this.whiteListDomains = response;

      this.totalDomains = this.whiteListDomains.length;

      setTimeout(() => {
        this.globals.loading = false;
      }, 1000 );

    })
  };

}
