import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';

import { ReactiveFormsModule } from '@angular/forms';

import { WhiteListService } from './services/white-list.service';

import { WhiteListUserService } from './services/white-list-user.service';

import { NavigationModule } from './navigation/navigation.module';
import { WhiteListDomainsModule } from './white-list-domains/white-list-domains.module';
import { WhiteListUsersModule } from './white-list-users/white-list-users.module';
import { AdminRoutingModule } from './admin-routing.module';
import {
  AddNewUserModalComponent 
} from './add-new-user-modal/add-new-user-modal.component';

import { 
  UserSubmitFeedbackComponent
} from './feedback/user-submit-feedback.component';

import { 
  UploadBatchFileModalComponent 
} from './upload-batch-file-modal/upload-batch-file-modal.component';

import { ModalModule } from 'ngx-bootstrap';
import { InvalidRecordsValidationComponent } from './upload-batch-file-modal/invalid-records-validation/invalid-records-validation.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    WhiteListDomainsModule,
    WhiteListUsersModule,
    NavigationModule,
    AdminRoutingModule,
  ],

  declarations: [
    AdminComponent,
    AddNewUserModalComponent,
    UserSubmitFeedbackComponent,
    UploadBatchFileModalComponent,
    InvalidRecordsValidationComponent
  ],

  exports: [
    AdminComponent
  ],

  providers:[
    WhiteListService,
    WhiteListUserService
  ],

  entryComponents:[
    AddNewUserModalComponent,
    UserSubmitFeedbackComponent,
    UploadBatchFileModalComponent,
    InvalidRecordsValidationComponent

  ]
})

export class AdminModule { }
