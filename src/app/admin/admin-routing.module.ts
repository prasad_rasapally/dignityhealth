import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { 
  WhiteListDomainsComponent 
} from './white-list-domains/white-list-domains.component';

import { 
  WhiteListUsersComponent 
} from './white-list-users/white-list-users.component';

const adminRoutes: Routes = [

  { path: 'whitelist-domains',  component: WhiteListDomainsComponent },

  { path: 'whitelist-users', component: WhiteListUsersComponent }

];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],

  exports: [
    RouterModule
  ],

  providers: []
})

export class AdminRoutingModule {}