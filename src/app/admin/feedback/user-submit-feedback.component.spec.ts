import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSubmitFeedbackComponent } from './user-submit-feedback.component';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

describe('UserSubmitFeedbackComponent', () => {
  let component: UserSubmitFeedbackComponent;
  let fixture: ComponentFixture<UserSubmitFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSubmitFeedbackComponent ],
      providers : [
        BsModalRef
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSubmitFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
